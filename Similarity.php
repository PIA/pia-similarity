<?php

class Similarity
{

    protected $threshold;
    protected $port;


    public function __construct(float $threshold = 0.65, int $port = 4562)
    {
        $this->threshold = $threshold;
        $this->port = $port;
    }

    protected function getEndpoint() {
        return 'http://131.152.217.186' . ':' . $this->port . '/api/v1/find/segments/similar';
    }

    public function search_by_signature()
    {
        print($this->query([]));
    }

    public function search_by_image_url($url)
    {
        $arrContextOptions = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $image_data = file_get_contents($url, false, stream_context_create($arrContextOptions));

        if ($image_data == false) {
            throw new Exception("Couldn't grab image.");
        }
            
        $image = imagecreatefromstring($image_data);  
        $image = imagescale($image, 299, 299);

        ob_start(); 

            imagepng($image);
            $image_output = ob_get_contents(); 

        ob_end_clean(); 
        
        $data = [
            "terms" => [
                [
                    "type" => "IMAGE",
                    "data" => "data:image/png;base64," . base64_encode($image_output),
                    "categories" => [
                        "visualtextcoembedding",
                    ],
                ],
            ],
        ];

        return $this->query($data);
    }

    public function search_by_image_upload($image)
    {
            
        $image = imagecreatefromstring(file_get_contents( $image['tmp_name'] ));  
        $image = imagescale($image, 299, 299);

        ob_start(); 

            imagepng($image);
            $image_output = ob_get_contents(); 

        ob_end_clean(); 
        
        $data = [
            "terms" => [
                [
                    "type" => "IMAGE",
                    "data" => "data:image/png;base64," . base64_encode($image_output),
                    "categories" => [
                        "visualtextcoembedding",
                    ],
                ],
            ],
        ];

        return $this->query($data);
    }

    public function search_by_text(string $text)
    {
        $data = [
            "terms" => [
                [
                    "type" => "TEXT",
                    "data" => $text,
                    "categories" => [
                        "mlclip",
                    ],
                ],
            ],
        ];

        return $this->query($data);
    }

    protected function query(array $data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->getEndpoint(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        // Execute the cURL request
        $response = curl_exec($curl);

        // Check if there was an error during the request
        if ($response === false) {
            $error = curl_error($curl);
            curl_close($curl);
            throw new Exception("Curl error: " . $error);
        }

        // Close the cURL session
        curl_close($curl);

        // Decode the JSON response
        $responseData = json_decode($response);

        // Check if decoding was successful
        if ($responseData === null) {
            throw new Exception("Invalid JSON response: " . $response);
        }

        // Debugging: print the response
        // var_dump($responseData); // Uncomment to inspect the response

        if (isset($responseData->results[0]->content)) {
            $passed = array_filter($responseData->results[0]->content, function ($item) {
                return ($item->value > $this->threshold);
            });

            $similar = [];

            foreach ($passed as $key => $item) {
                // Check if the key is numeric-only
                if (ctype_digit($item->key)) {
                    $signature = $item->key;  // Keep numeric keys as-is
                } else {
                    // Apply trimming for non-numeric keys
                    $signature = ltrim(rtrim($item->key, '_1'), 'i_');
                }

                $similar[] = [
                    "signature" => $signature,
                    "base_path" => str_split($signature, 6)[0],
                    "score" => $item->value
                ];
            }

            return $similar;
        } else {
            throw new Exception("Unexpected response format: " . var_export($responseData, true));
        }
    }
}
