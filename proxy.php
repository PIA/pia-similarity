<?php

require_once('Similarity.php');

header('Content-Type: application/json');
// CORS headers
header("Access-Control-Allow-Origin: *"); // Allow all origins (or specify a specific origin)
header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); // Allow specific HTTP methods
header("Access-Control-Allow-Headers: Content-Type"); // Allow specific headers

// Handle preflight requests (OPTIONS method)
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    exit;
}

$threshold = isset($_POST['threshold']) ? floatval($_POST['threshold']) : 0.65;
$port = 4562;

$similarity = new Similarity($threshold, $port);

$results = null;

try {
    if (isset($_POST['url']) && $_POST['url'] !== '') {
        // Handle image URL search
        $url = htmlspecialchars($_POST['url']);
        $results = $similarity->search_by_image_url($url);
    } elseif (isset($_FILES['image'])) {
        // Handle image upload search
        if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {
            $results = $similarity->search_by_image_upload($_FILES['image']);
        } else {
            throw new Exception("Image upload failed.");
        }
    } elseif (isset($_POST['text']) && $_POST['text'] !== '') {
        // Handle text search
        $text = htmlspecialchars($_POST['text']);
        $results = $similarity->search_by_text($text);
    } else {
        throw new Exception("No valid input provided.");
    }

    // Return the results as JSON
    echo json_encode([
        'status' => 'success',
        'data' => $results
    ]);
} catch (Exception $e) {
    // Handle any errors
    echo json_encode([
        'status' => 'error',
        'message' => $e->getMessage()
    ]);
}
