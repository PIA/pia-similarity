<?php

require_once('Similarity.php');

$url = '';
$text = '';
$threshold = 0.65;
$port = 4562;

if(isset($_REQUEST['threshold'])) {
    $threshold = floatval($_REQUEST['threshold']);
}

$similarity = new Similarity($threshold, $port);

if (isset($_REQUEST['url']) && $_REQUEST['url'] !== '') {
    $url = htmlspecialchars($_REQUEST['url']);
    $results = $similarity->search_by_image_url($url);
}

if (isset($_REQUEST['url']) && $_REQUEST['url'] !== '') {
    $url = htmlspecialchars($_REQUEST['url']);
    $results = $similarity->search_by_image_url($url);
}

if (isset($_FILES['image'])){
    if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {
        $results = $similarity->search_by_image_upload($_FILES['image']);
    }
}

if (isset($_REQUEST['text']) && $_REQUEST['text'] !== '') {
    $text = htmlspecialchars($_REQUEST['text']);
    $results = $similarity->search_by_text($text);
}

?>

<?php if(!isset($_REQUEST['signatures_only'])): ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PIA Similarity</title>

    <script src="https://cdn.tailwindcss.com"></script>

</head>

<body class="bg-gray-800 text-white p-4">

    <div class="block md:flex mb-10">
        <form action="/" method="POST" enctype="multipart/form-data" class="w-full md:w-1/2 pr-2">

            <label for="url">Enter an image URL…</label>
            <input type="text" name="url" placeholder="Enter URL for an image" value="<?= $url ?>" class="block w-full p-1 px-2 mb-2 text-black">

            <label for="image">…or upload an image file</label>
            <input type="file" name="image" accept="image/*" class="block mb-2">

            <label for="url">…or seach by text.</label>
            <input type="text" name="text" placeholder="Enter text to search for" value="<?= $text ?>" class="block w-full p-1 px-2 mb-2 text-black">

            <label for="threshold">Score Threshold: <span id="threshold"><?= $threshold ?></span></label>
            <input type="range" name="threshold" min="0.1" max="0.95" step="0.05" value="<?= $threshold ?>" class="block w-full md:w-96 mb-2" oninput="document.querySelector('#threshold').innerHTML = this.value">

            <div>
                <button type="submit" class="p-1 px-2 mr-2 border border-white">Search</button>
                <button type="reset" class="underline">Clear Form</button>
            </div>

        </form>
    </div>

    <?php if(isset($results)): ?>
        <a target="_blank" href="https://participatory-archives.ch/s/explore/item?fulltext_search=<?php foreach ($results as $key => $i) : ?><?= $i['signature'] ?>+<?php endforeach; ?>" class="p-1 px-2 mr-2 border border-blue-600 bg-blue-600 mb-4 inline-block">Open Results in <i>PIA Explore</i></a>
    <?php endif; ?>

    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-2">
        <?php if(isset($results)): ?>
            <?php foreach ($results as $key => $i) : ?>
                <div class="grid-item">
                    <a href="https://participatory-archives.ch/s/explore/item?fulltext_search=<?= $i['signature'] ?>">
                        <img src="https://sipi.participatory-archives.ch/<?= $i['base_path'] ?>/<?= $i['signature'] ?>.jp2/full/800,/0/default.jpg">
                    </a>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p>Nothing found.</p>
        <?php endif; ?>
    </div>

</body>

</html>
<?php else: ?>
    <?php if(isset($results)): ?>
        <?php foreach ($results as $key => $i) {
            $signatures[] = $i['signature'];
        } ?>
        <?= join(',', $signatures) ?>
    <?php endif; ?>
<?php endif; ?>